import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Row } from 'reactstrap';
import "../App.css";
import { useEffect, useState } from 'react';

// hàm kiểm tra email đúng định dạng ko?
const validateEmail = (paramEmail) => {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

const HookSignUp = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [validate, setValidate] = useState(false);

    const onFirstnameInputChange = (event) => {
        setFirstname(event.target.value);
    }

    useEffect(() => {
        var vVal = true;

        if (firstname == "") vVal = false;

        if (lastname == "") vVal = false;

        if (password.length < 3) vVal = false;

        if (validateEmail(email) == false) vVal = false;

        console.log("validate: " + vVal);
        setValidate(vVal);

    }, [firstname, lastname, email, password])

    const onLastnameInputChange = (event) => {
        var vLastname = event.target.value;
        setLastname(vLastname);
    }



    const onEmailInputChange = (event) => {
        var vEmail = event.target.value;
        setEmail(vEmail);
    }

    const onPassInputChange = (event) => {
        var vPass = event.target.value;
        setPassword(vPass);
    }

    const onBtnGetClicked = () => {
        if (validate) {
            console.log("Firstname: " + firstname);
            console.log("Lastname: " + lastname);
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
    }

    return (
        <div>
            <Row>
                <div className='text-welcome'>Sign Up for Free</div>
            </Row>

            <Row>
                <Col>
                    <label>
                        <span>First Name <span>*</span></span>
                        <input type="text" required value={firstname} onChange={onFirstnameInputChange} />
                    </label>
                </Col>
                <Col>
                    <label>
                        <span>Last Name <span>*</span></span>
                        <input type="text" required value={lastname} onChange={onLastnameInputChange} />
                    </label>
                </Col>
            </Row>
            <label style={{ marginTop: "1.8rem" }}>
                <span>Email Address <span>*</span></span>
                <input type="text" required value={email} onChange={onEmailInputChange} />
            </label>
            <label style={{ marginTop: "1.8rem" }}>
                <span>Set A Password <span>*</span></span>
                <input type="text" required value={password} onChange={onPassInputChange} />
            </label>
            <div className='text-forgot'>Forgot password?</div>
            <button className='btn-log' onClick={onBtnGetClicked}>GET STARTED</button>
        </div>
    );
}

export default HookSignUp;