import { Button, Col, Row } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import HookSignUp from "./HookSignUp";
import HookLogin from "./HookLogin";
import { useState } from "react";

const EntryComponent = () => {
    const [showSignup, setShowSignup] = useState(false);

    const onSignupBtnClick = () => {
        setShowSignup(true);
    }

    const onLoginBtnClick = () => {
        setShowSignup(false);
    }

    return (
        <div className='login-container'>
            <Row>
                <Col md="6" style={{ paddingRight: "0px" }}>
                    <Button className='login-btn' 
                        style={{ backgroundColor: showSignup ? "#1ab089" : "#425358", color: showSignup ? "white" : "#8f9d98" }}
                        onClick={onSignupBtnClick}
                    >Sign up</Button>
                </Col>
                <Col md="6" style={{ paddingLeft: "0px" }}>
                    <Button className='login-btn' 
                        style={{ backgroundColor: showSignup ? "#425358" : "#1ab089", color: showSignup ? "#8f9d98" : "white" }}
                        onClick={onLoginBtnClick}
                    >Log in</Button>
                </Col>
            </Row>

            {
                showSignup ? <HookSignUp/> : <HookLogin/>
            }
        </div>
    );
}

export default EntryComponent;