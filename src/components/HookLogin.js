import 'bootstrap/dist/css/bootstrap.min.css';
import { Row } from 'reactstrap';
import "../App.css";
import { useEffect, useState } from 'react';

// hàm kiểm tra email đúng định dạng ko?
const validateEmail = (paramEmail) => {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

//hàm validate password (pass phải từ 3 kí tự trở lên)
const validatePass = (paramPass) => {
    if (paramPass.length < 3) {
        return false;
    } else {
        return true;
    }
}

const HookLogin = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [emailValidate, setEVa] = useState(false);
    const [passValidate, setPVa] = useState(false);

    const onEmailInputChange = (event) => {
        setEmail(event.target.value);
    }

    const onPassInputChange = (event) => {
        setPassword(event.target.value);
    }

    useEffect(() => {
        console.log(email);
        setEVa(validateEmail(email));
    }, [email]);

    useEffect(() => {
        console.log(emailValidate);
    }, [emailValidate]);

    useEffect(() => {
        console.log(password);
        setPVa(validatePass(password));
    }, [password]);

    useEffect(() => {
        console.log(passValidate);
    }, [passValidate]);

    const onBtnLogClicked = () => {
        if (emailValidate && passValidate) {
            console.log("Email: " + email);
            console.log("Password: " + password);
        }
    }

    return (
        <div>
            <Row>
                <div className='text-welcome'>Welcome Back!</div>
            </Row>
            <label>
                <span>Email Address <span>*</span></span>
                <input type="text" required value={email} onChange={onEmailInputChange} />
            </label>
            <label style={{ marginTop: "1.8rem" }}>
                <span>Password <span>*</span></span>
                <input type="text" required value={password} onChange={onPassInputChange} />
            </label>
            <div className='text-forgot'>Forgot password?</div>
            <button className='btn-log' onClick={onBtnLogClicked}>LOG IN</button>
        </div>
    );
}

export default HookLogin;