import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container, Row } from "reactstrap";
import '../App.css';

const FormComponent = () => {
    return (
        <Container className="jumbotron">
            <Row>
                <Col >
                    <h4 class="h4 text-center">HỒ SƠ NHÂN VIÊN</h4>
                </Col>
            </Row>

            <Row className='mt-4'>
                <Col sm="8">
                    <div class="row form-group" style={{ marginBottom: "1rem" }}>
                        <Col sm="3">
                            <label class="label">Họ và tên</label>
                        </Col>
                        <Col sm="9">
                            <input class="form-control" placeholder="..." />
                        </Col>
                    </div>

                    <div class="row form-group" style={{ marginBottom: "1rem" }}>
                        <Col sm="3">
                            <label class="label">Ngày sinh</label>
                        </Col>
                        <Col sm="9">
                            <input class="form-control" placeholder="..." />
                        </Col>
                    </div>

                    <div class="row form-group" style={{ marginBottom: "1rem" }}>
                        <Col sm="3">
                            <label class="label">Số điện thoại</label>
                        </Col>
                        <Col sm="9">
                            <input class="form-control" placeholder="..." />
                        </Col>
                    </div>

                    <div class="row form-group" style={{ marginBottom: "1rem" }}>
                        <Col sm="3">
                            <label class="label">Giới tính</label>
                        </Col>
                        <Col sm="9">
                            <input class="form-control" placeholder="..." />
                        </Col>
                    </div>
                </Col>
                <Col sm="4" style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <img src={require("../assets/images/avatar04.png")} class="img-thumbnail" />
                </Col>
            </Row>

            <Row className='form-group mt-4'>
                <Col sm="2">
                    <label class="label">Công việc</label>
                </Col>
                <Col sm="10">
                    <textarea class="form-control" placeholder="..."></textarea>
                </Col>
            </Row>

            <Row className='mt-4'>
                <Col style={{ textAlign: "end" }}>
                    <button class="btn btn-success" style={{ marginRight: "1rem" }}>Chi tiết</button>
                    <button class="btn btn-success">Kiểm tra</button>
                </Col>
            </Row>
        </Container>
    );
} 

export default FormComponent;