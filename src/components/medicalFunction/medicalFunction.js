import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Nav, Navbar, NavbarBrand, Row } from 'reactstrap';
import './style.css'

const MedicalFunction = () => {
    return (
        <div className='bg'>
            <Navbar className='container'>
                <div style={{ display: "flex" }}>
                    <NavbarBrand className='nav-brand'>MedicalFunc</NavbarBrand>
                    <div style={{ display: "flex", marginLeft: "3rem" }}>
                        <div className='menu'>Home</div>
                        <div className='menu'>Product</div>
                        <div className='menu'>Pricing</div>
                        <div className='menu'>Contact</div>
                    </div>
                </div>
                <div style={{ display: "flex" }}>
                    <button className='log-btn'>Login</button>
                    <button className='join-btn'>JOIN US</button>
                </div>
            </Navbar>

            <Row>
                <Col md="6">
                    <div style={{width: "90%", margin: "10rem 0rem 10rem auto"}}>
                        <div className='text-wel'>Welcome</div>
                        <div className='text-on'>Online Appoinment</div>
                        <div className='text-med'>Medical Functional is most focused in helping you discover your most beauiful smile</div>
                        <div style={{ display: "flex" }}>
                            <button className='get-btn'>Get Quote Now</button>
                            <button className='learn-btn'>Learn More</button>
                        </div>
                    </div>
                </Col>
                <Col md="6" style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <img src={require("../../assets/images/none.png")} style={{ width: "90%" }}></img>
                </Col>
            </Row>
        </div>
    );
}

export default MedicalFunction;