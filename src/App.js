import EntryComponent from "./components/EntryComponent";
import FormComponent from "./components/formComponent";
import MedicalFunction from "./components/medicalFunction/medicalFunction";

function App() {
  return (
    // <FormComponent/>

    //<MedicalFunction/>

    <div style={{textAlign: "center", paddingTop: "2rem"}}>
      <EntryComponent/>
    </div>
  );
}

export default App;
